package br.com.bancointer.github.util;

import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import br.com.bancointer.github.R;

public class ImageUtil {
    
    public static void loadCircleImage(final ImageView imageView, String url) {
        if (imageView != null && imageView.getContext() != null) {
            try {
                if (url != null && url.length() > 0) {
                    Glide.with(imageView.getContext()).load(url).asBitmap().placeholder(R.drawable.ic_action_user).error(R.drawable.ic_action_user).centerCrop()
                         .diskCacheStrategy(DiskCacheStrategy.RESULT)
                         .into(new BitmapImageViewTarget(imageView) {
                             @Override
                             protected void setResource(Bitmap resource) {
                                 RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(imageView.getContext().getResources(), resource);
                                 circularBitmapDrawable.setCircular(true);
                                 imageView.setImageDrawable(circularBitmapDrawable);
                             }
                         });
                } else {
                    Glide.with(imageView.getContext()).load("").placeholder(R.mipmap.ic_launcher).into(imageView);
                }
            } catch (IllegalArgumentException e) {
                // log
            }
        }
    }
}
