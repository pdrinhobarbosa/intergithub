package br.com.bancointer.github.viewholder

import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import br.com.bancointer.github.R
import br.com.bancointer.github.model.Repository
import br.com.bancointer.github.util.ImageUtil
import com.jude.easyrecyclerview.adapter.BaseViewHolder

class RepositoryViewHolder(parent: ViewGroup) : BaseViewHolder<Repository>(parent, R.layout.repository_list_row) {

    private val txtRepository: TextView = `$`(R.id.txt_repository)
    private val txtUser: TextView = `$`(R.id.txt_user)
    private val txtStars: TextView = `$`(R.id.txt_stars)
    private val txtForks: TextView = `$`(R.id.txt_forks)
    private val imgUser: ImageView = `$`(R.id.img_user)

    override fun setData(repository: Repository) {
        txtRepository.text = repository.name
        txtUser.text = repository.user.login
        txtStars.text = "${repository.starsCount}"
        txtForks.text = "${repository.forksCount}"

        ImageUtil.loadCircleImage(imgUser, repository.user.avatarUrl)
    }
}