package br.com.bancointer.github.adapter

import android.content.Context
import android.view.ViewGroup
import br.com.bancointer.github.model.PullRequest
import br.com.bancointer.github.viewholder.PullRequestViewHolder
import com.jude.easyrecyclerview.adapter.BaseViewHolder
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter

class PullRequestAdapter(context: Context) : RecyclerArrayAdapter<PullRequest>(context) {

    override fun OnCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return PullRequestViewHolder(parent)
    }

    override fun add(`object`: PullRequest?) {
        val indexOf = mObjects.indexOf(`object`)
        if (indexOf < 0) {
            mObjects.add(`object`)
        } else {
            mObjects[indexOf] = `object`
        }
    }
}