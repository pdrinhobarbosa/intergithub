package br.com.bancointer.github.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PullRequest extends BaseEntity {
    
    private String title;
    
    @JsonProperty("body")
    private String description;
    
    @JsonProperty("html_url")
    private String htmlUrl;
    
    private User user;
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public User getUser() {
        return user;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
    
    public String getHtmlUrl() {
        return htmlUrl;
    }
    
    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }
}
