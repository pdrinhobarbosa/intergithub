package br.com.bancointer.github.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class User extends BaseEntity {
    
    private String login;
    
    @JsonProperty("avatar_url")
    private String avatarUrl;
    
    public String getLogin() {
        return login;
    }
    
    public void setLogin(String login) {
        this.login = login;
    }
    
    public String getAvatarUrl() {
        return avatarUrl;
    }
    
    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
