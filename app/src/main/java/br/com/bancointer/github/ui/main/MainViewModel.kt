package br.com.bancointer.github.ui.main

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import br.com.bancointer.github.model.vo.RepositoryJsonVO
import com.bolao.repository.GitHubRepository

class MainViewModel : ViewModel() {

    fun getRepositoryLiveData(): LiveData<RepositoryJsonVO> {
        return GitHubRepository.getRepositoryJsonData()
    }

    fun search(language: String, sort: String, page: Int) {
        GitHubRepository.search(language, sort, page)
    }
}
