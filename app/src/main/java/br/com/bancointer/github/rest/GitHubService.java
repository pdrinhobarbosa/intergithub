package br.com.bancointer.github.rest;

import java.util.List;

import br.com.bancointer.github.model.PullRequest;
import br.com.bancointer.github.model.vo.RepositoryJsonVO;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GitHubService {
    
    @GET("search/repositories")
    Call<RepositoryJsonVO> searchRepositories(@Query("q") String language, @Query("sort") String sort, @Query("page") int page);
    
    @GET("repos/{owner}/{repository}/pulls")
    Call<List<PullRequest>> listPullRequests(@Path("owner") String owner, @Path("repository") String repository);
}
