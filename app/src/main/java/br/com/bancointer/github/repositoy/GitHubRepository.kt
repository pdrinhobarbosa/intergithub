package com.bolao.repository

import android.arch.lifecycle.MutableLiveData
import br.com.bancointer.github.model.PullRequest
import br.com.bancointer.github.model.vo.RepositoryJsonVO
import br.com.bancointer.github.rest.GitHubService
import br.com.bancointer.github.util.RetrofitUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object GitHubRepository {

    val service = RetrofitUtil.getRetrofit().create(GitHubService::class.java)

    val repositoryJsonLiveData = MutableLiveData<RepositoryJsonVO>()
    val pullRequestsLiveData = MutableLiveData<List<PullRequest>>()

    fun getRepositoryJsonData(): MutableLiveData<RepositoryJsonVO> {
        return repositoryJsonLiveData
    }

    fun search(language: String, sort: String, page: Int) {
        val call = service.searchRepositories(language, sort, page)
        call.enqueue(object : Callback<RepositoryJsonVO> {
            override fun onResponse(call: Call<RepositoryJsonVO>, response: Response<RepositoryJsonVO>) {
                if (response.code() == 200) {
                    val repositoryJsonVO = response.body()
                    if (repositoryJsonVO != null) {
                        repositoryJsonLiveData.value = repositoryJsonVO
                    }
                } else {
                    repositoryJsonLiveData.value = null
                }

            }

            override fun onFailure(call: Call<RepositoryJsonVO>, t: Throwable) {
                repositoryJsonLiveData.value = null
            }
        })
    }

    fun getPullRequestsData(): MutableLiveData<List<PullRequest>> {
        return pullRequestsLiveData
    }

    fun loadPullRequests(owner: String, repository: String) {
        val call = service.listPullRequests(owner, repository)
        call.enqueue(object : Callback<List<PullRequest>> {
            override fun onResponse(call: Call<List<PullRequest>>, response: Response<List<PullRequest>>) {
                if (response.code() == 200) {
                    val list = response.body()
                    if (list != null) {
                        pullRequestsLiveData.value = list
                    }
                } else {
                    pullRequestsLiveData.value = null
                }

            }

            override fun onFailure(call: Call<List<PullRequest>>, t: Throwable) {
                pullRequestsLiveData.value = null
            }
        })
    }

}