package br.com.bancointer.github.ui.pullrequest

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import br.com.bancointer.github.R
import br.com.bancointer.github.adapter.PullRequestAdapter
import br.com.bancointer.github.model.Repository
import br.com.bancointer.github.ui.main.PullRequestViewModel
import com.jude.easyrecyclerview.EasyRecyclerView


class PullRequestListFragment : Fragment() {

    companion object {
        fun newInstance() = PullRequestListFragment()
    }

    private lateinit var viewModel: PullRequestViewModel
    private lateinit var recyclerView: EasyRecyclerView
    private lateinit var pullRequestAdapter: PullRequestAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(PullRequestViewModel::class.java)

        val repositoryLiveData = viewModel.getPullRequestsLiveData()
        repositoryLiveData.observe(this, Observer { list ->
            recyclerView.setRefreshing(false)

            if (list != null) {
                pullRequestAdapter.addAll(list)
            }
        })
    }

    override fun onResume() {
        super.onResume()

        val appCompatActivity = activity as AppCompatActivity
        appCompatActivity.supportActionBar?.title = getString(R.string.pull_requests)

        appCompatActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.pull_request_list_fragment, container, false)

        recyclerView = view.findViewById(R.id.recyclerView)

        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.reverseLayout = false

        recyclerView.setLayoutManager(linearLayoutManager)

        pullRequestAdapter = PullRequestAdapter(context!!)

        recyclerView.adapter = pullRequestAdapter

        pullRequestAdapter.setOnItemClickListener { position ->
            val pullRequest = pullRequestAdapter.getItem(position)

            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(pullRequest.htmlUrl)
            startActivity(i)
        }

        recyclerView.setRefreshing(true)

        val repository = arguments?.getSerializable(Repository::class.java.simpleName) as Repository?
        if (repository != null) {
            load(repository)
        }

        setHasOptionsMenu(true)

        return view
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            fragmentManager?.popBackStack()

            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

    private fun load(repository: Repository) {
        viewModel.load(repository.user.login, repository.name)
    }


}
