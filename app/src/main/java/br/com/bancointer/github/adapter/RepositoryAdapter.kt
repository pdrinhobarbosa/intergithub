package br.com.bancointer.github.adapter

import android.content.Context
import android.view.ViewGroup
import br.com.bancointer.github.model.Repository
import br.com.bancointer.github.viewholder.RepositoryViewHolder
import com.jude.easyrecyclerview.adapter.BaseViewHolder
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter

class RepositoryAdapter(context: Context) : RecyclerArrayAdapter<Repository>(context) {

    override fun OnCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return RepositoryViewHolder(parent)
    }

    override fun add(`object`: Repository?) {
        val indexOf = mObjects.indexOf(`object`)
        if (indexOf < 0) {
            mObjects.add(`object`)
        } else {
            mObjects[indexOf] = `object`
        }
    }
}