package br.com.bancointer.github.ui.main

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.bancointer.github.MainActivity
import br.com.bancointer.github.R
import br.com.bancointer.github.adapter.RepositoryAdapter
import br.com.bancointer.github.model.Repository
import com.jude.easyrecyclerview.EasyRecyclerView
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var replaceFragmentListener: ReplaceFragmentListener

    private lateinit var viewModel: MainViewModel
    private lateinit var recyclerView: EasyRecyclerView
    private lateinit var repositoryAdapter: RepositoryAdapter

    private var page = 1

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (activity is MainActivity) {
            replaceFragmentListener = activity as ReplaceFragmentListener
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.main_fragment, container, false)

        recyclerView = view.findViewById(R.id.recyclerView)

        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.reverseLayout = false

        recyclerView.setLayoutManager(linearLayoutManager)

        repositoryAdapter = RepositoryAdapter(context!!)

        recyclerView.adapter = repositoryAdapter

        repositoryAdapter.setMore(R.layout.view_progress, object : RecyclerArrayAdapter.OnMoreListener {
            override fun onMoreShow() {
                search(page++)
            }

            override fun onMoreClick() {

            }
        })

        repositoryAdapter.setOnItemClickListener { position ->
            val repository = repositoryAdapter.getItem(position)

            replaceFragmentListener.showPullRequestList(repository)
        }

        return view
    }

    override fun onStart() {
        super.onStart()

        search(page)
    }

    override fun onResume() {
        super.onResume()

        val appCompatActivity = activity as AppCompatActivity
        appCompatActivity.supportActionBar?.title = getString(R.string.repository_list)

        appCompatActivity.supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

    private fun search(page: Int) {
        recyclerView.setRefreshing(true)

        viewModel.search("language:kotlin,java", "stars", page)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        val repositoryLiveData = viewModel.getRepositoryLiveData()
        repositoryLiveData.observe(this, Observer { repositoryJsonVO ->
            recyclerView.setRefreshing(false)

            if (repositoryJsonVO != null) {
                val items = repositoryJsonVO.items
                if (items != null) {
                    repositoryAdapter.addAll(items)
                }
            }
        })
    }

    interface ReplaceFragmentListener {
        fun showPullRequestList(repository: Repository)
    }
}
