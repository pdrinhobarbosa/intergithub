package br.com.bancointer.github

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import br.com.bancointer.github.model.Repository
import br.com.bancointer.github.ui.main.MainFragment
import br.com.bancointer.github.ui.pullrequest.PullRequestListFragment

class MainActivity : AppCompatActivity(), MainFragment.ReplaceFragmentListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, MainFragment.newInstance())
                    .commitNow()
        }
    }

    override fun showPullRequestList(repository: Repository) {
        val fragment = PullRequestListFragment.newInstance()

        val args = Bundle()
        args.putSerializable(Repository::class.java.simpleName, repository)

        fragment.arguments = args

        supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit()
    }
}
