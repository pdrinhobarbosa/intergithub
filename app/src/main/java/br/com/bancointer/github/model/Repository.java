package br.com.bancointer.github.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Repository extends BaseEntity {
    
    private static final long serialVersionUID = 8737558769920121420L;
    
    private String name;
    
    @JsonProperty("full_name")
    private String fullName;
    
    @JsonProperty("html_url")
    private String htmlUrl;
    
    @JsonProperty("forks_count")
    private Long forksCount;
    
    @JsonProperty("forks_url")
    private String forksUrl;
    
    @JsonProperty("stargazers_count")
    private Long starsCount;
    
    @JsonProperty("owner")
    private User user;
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getFullName() {
        return fullName;
    }
    
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
    
    public String getHtmlUrl() {
        return htmlUrl;
    }
    
    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }
    
    public Long getForksCount() {
        return forksCount;
    }
    
    public void setForksCount(Long forksCount) {
        this.forksCount = forksCount;
    }
    
    public String getForksUrl() {
        return forksUrl;
    }
    
    public void setForksUrl(String forksUrl) {
        this.forksUrl = forksUrl;
    }
    
    public Long getStarsCount() {
        return starsCount;
    }
    
    public void setStarsCount(Long starsCount) {
        this.starsCount = starsCount;
    }
    
    public User getUser() {
        return user;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
}
