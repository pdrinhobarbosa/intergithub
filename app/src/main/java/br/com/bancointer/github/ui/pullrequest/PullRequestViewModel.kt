package br.com.bancointer.github.ui.main

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import br.com.bancointer.github.model.PullRequest
import com.bolao.repository.GitHubRepository

class PullRequestViewModel : ViewModel() {

    fun getPullRequestsLiveData(): LiveData<List<PullRequest>> {
        return GitHubRepository.getPullRequestsData()
    }

    fun load(owner: String, repository: String) {
        GitHubRepository.loadPullRequests(owner, repository)
    }
}
