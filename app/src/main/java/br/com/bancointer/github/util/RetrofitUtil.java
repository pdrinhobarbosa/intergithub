package br.com.bancointer.github.util;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class RetrofitUtil {
    
    private static Retrofit retrofit;
    
    public static Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.GIT_HUB_API_BASE_URL)
                    .addConverterFactory(JacksonConverterFactory.create())
                    .build();
        }
        
        return retrofit;
    }
}
