package br.com.bancointer.github.viewholder

import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import br.com.bancointer.github.R
import br.com.bancointer.github.model.PullRequest
import br.com.bancointer.github.util.ImageUtil
import com.jude.easyrecyclerview.adapter.BaseViewHolder

class PullRequestViewHolder(parent: ViewGroup) : BaseViewHolder<PullRequest>(parent, R.layout.pull_request_list_row) {

    private val txtUser: TextView = `$`(R.id.txt_user)
    private val txtTitle: TextView = `$`(R.id.txt_title)
    private val txtDescription: TextView = `$`(R.id.txt_description)
    private val imgUser: ImageView = `$`(R.id.img_user)

    override fun setData(pullRequest: PullRequest) {
        txtTitle.text = pullRequest.title
        txtUser.text = pullRequest.user.login

        ImageUtil.loadCircleImage(imgUser, pullRequest.user.avatarUrl)

        txtDescription.text = if (pullRequest.description.isNotEmpty()) {
            pullRequest.description
        } else {
            context.getString(R.string.no_description)
        }
    }
}