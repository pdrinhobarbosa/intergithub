package br.com.bancointer.github.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

import br.com.bancointer.github.model.Repository;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RepositoryJsonVO {
    
    private Long totalCount;
    private List<Repository> items;
    
    public Long getTotalCount() {
        return totalCount;
    }
    
    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }
    
    public List<Repository> getItems() {
        return items;
    }
    
    public void setItems(List<Repository> items) {
        this.items = items;
    }
}
